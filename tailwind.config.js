const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./templates/**/*.html"],
    darkMode: 'class',
    theme: {
        screens: {
            'sm': '480px',
            // => @media (min-width: 640px) { ... }

            'md': '640px',
            // => @media (min-width: 768px) { ... }

            'lg': '768px',
            // => @media (min-width: 1024px) { ... }

            'xl': '1024px',
            // => @media (min-width: 1280px) { ... }

            '2xl': '1536px',
            // => @media (min-width: 1536px) { ... }
        },
        extend: {
            colors: {
                'base00': '#282a36',
                'base01': '#34353e',
                'base02': '#43454f',
                'base03': '#78787e',
                'base04': '#a5a5a9',
                'base05': '#e2e4e5',
                'base06': '#eff0eb',
                'base07': '#f1f1f0',
                'base08': '#ff5c57',
                'base09': '#ff9f43',
                'base0A': '#f3f99d',
                'base0B': '#5af78e',
                'base0C': '#9aedfe',
                'base0D': '#57c7ff',
                'base0E': '#ff6ac1',
                'base0F': '#b2643c',
            },
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/typography'),
    ],
};

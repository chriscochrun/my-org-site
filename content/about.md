+++
title = "About Me"
author = ["Chris Cochrun"]
date = 2023-05-05T00:00:00-05:00
draft = false
[taxonomies]
+++

## Overview {#overview} {#overview-overview}

Hi my name is Chris and I'm a youth minister in Kansas. I have a lot of ideas about God, technology, and what folks should be considering instead of the status quo regarding their choices in life. Here are some of those thoughts. <br/>


## Links {#links} {#links-links}

-   [Gitlab](https://gitlab.com/chriscochrun) <br/>
-   [Github](https://github.com/chriscochrun) <br/>

{{ videos(id="8BVMcuIGiAA") }} <br/>


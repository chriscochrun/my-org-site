#+TITLE: Building Your Own Site
#+AUTHOR: Chris Cochrun
#+DATE:<2024-12-19 Thu> 

* Why not?
Mostly people don't do it because it's hard and takes time for not a lot of gains. But I think it's so cool to have your own place on the internet!

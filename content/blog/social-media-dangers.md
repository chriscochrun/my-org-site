+++
title = "Social Media and Capitalism"
date = 2023-05-04T00:00:00-05:00
draft = false
[taxonomies]
+++

## Our World is Changing {#our-world-is-changing}

So many of us are discovering that our world is changing more and more and not for the better. For some reason, humans have always viewed progression and new things as a good thing. In most cases, this is true. The printing press brought knowledge to the masses, trains and cars bridged long distances and made trade and commerce boom, computers made complex systems trivial to establish, but with the internet and especially social media, so many of us are more and more wishing things could be different. We'd all like to see the world return to more of the way it used to be. <br/>

It's my presonal belief that we all lack an understanding of what is happening psychologically and monetarily every time we browse Facebook or Instagram, every time we look at our phones, every time we open our laptop lids. We don't really know what happens behind the scenes on every click, tap and scroll. If we did, we may use our technology differently. <br/>


## How do you vote? {#how-do-you-vote}

In America, we have a democratic voting system for our government, and if you recall in high school government classes, throughout early parts of US history, voting was a huge deal to give power to the people and keep it out of the hands of a few powerful people. US founders largely believed that power in the hands of a few is extremely dangerous and not at all a good thing. It is often much better for the individual to have as much power as possible without infringing on the rights of others so that they can make the best decisions for themselves. However, making sure an entire country is actually able to function is hard so, a few still need power to make sure things get done in an orderly fashion. That's the balance that is required to walk in a republic government. <br/>

In the last century with a constant boom in commerce thanks to advancements in manuafacturing and trade, we've seen a different kind of vote pop up more and more, the vote of money. Again in the US, we largely have a capitalistic economy, when one company does well, they sell more, when they sell more, they get stronger and on it goes. In these more recent years of our US history, we've understood that when we purchase something, we are in support of that company succeeding. Not everyone understands this as we live in a hyper consumerist society, but when we do, we may pause a little more often and make decisions based off this information. You may choose to take your car to your brothers auto shop instead of the generic dealership because he's your brother, and therefore you care more that his business succeeds than the dealership you may have taken your care to. You may purchase things from more local mom and pop stores than Amazon when you can because you care for those local places. Not always, but some of us understand this and do it. <br/>

I'd argue now, we have shifted into an even more powerful vote in the last decade. While you could vote once a year on your elected officials that was a fairly infrequent opportunity to have a voice. Then we get the dollar. A method of voting everyday, even multiple times a day! Now our voice can echo at an even higher rate by our vote with our attention. Social media has shifted our world to a constant grab for our attention. Every glance we make on the internet, whether it's through a browser, app, or game, is a powerful vote for the companies and organizations that make the content you are looking at possible. Especially on social media, even how long we stop to look at something is a vote. Every moment you spend on social media is giving Facebook, Twitter, and Youtube a resounding YES for grabbing your attention. They acquire money by your eyes on their platforms. It's this vote that changes our world. We have the power to control it, but they have mastered the ability to trick us anyway. <br/>

I'd argue, that we have to understand this if we are to protect the power of individuals. Else we lose that power, and are slowly becoming the cattle that are herded to purchase, think, and act as those in control should will. <br/>

Choose a good place to vote for. <br/>


+++
title = "Building Your Own Site"
author = ["Chris Cochrun"]
date = 2024-12-19T00:00:00-06:00
draft = false
[taxonomies]
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Why not?](#why-not)

</div>
<!--endtoc-->



## Why not? {#why-not}

Mostly people don't do it because it's hard and takes time for not a lot of gains. But I think it's so cool to have your own place on the internet! <br/>


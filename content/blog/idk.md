+++
title = "My Ramblings"
author = ["Chris Cochrun"]
date = 2023-05-04T00:00:00-05:00
draft = false
[taxonomies]
+++

## Overview {#overview}

Hi my name is Chris and I'm a youth minister in Kansas. I have a lot of ideas about God, technology, and what folks should be considering instead of the status quo regarding their choices in life. Here are some of those thoughts. <br/>


## Links {#links}

-   [Social Media]({{< relref "social-media-dangers" >}}) <br/>


## Something {#something}

```rust
fn main() {
    println!("Hello World!");
}
```

<div class="videos">

Something <br/>

</div>


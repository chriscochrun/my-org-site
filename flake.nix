{
  description = "my site";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    naersk.url = "github:nix-community/naersk";
    flake-utils.url = "github:numtide/flake-utils";
    fenix.url = "github:nix-community/fenix";
  };

  outputs = inputs: with inputs;
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [fenix.overlays.default];
            # overlays = [cargo2nix.overlays.default];
          };
          naersk' = pkgs.callPackage naersk {};

          nbi = with pkgs; [
            alejandra
            (pkgs.fenix.stable.withComponents [
              "cargo"
              "clippy"
              "rust-src"
              "rustc"
              "rustfmt"
            ])
            rust-analyzer
          ];

          bi = with pkgs; [
            zola
            tailwindcss
            nodejs
            just
          ];
        in rec
        {
          devShell = pkgs.mkShell {
            nativeBuildInputs = nbi;
            buildInputs = bi;
          };
          defaultPackage = naersk'.buildPackage {
            src = ./.;
          };
          packages = {
            default = naersk'.buildPackage {
              src = ./.;
            };
          };
        }
      );
}
